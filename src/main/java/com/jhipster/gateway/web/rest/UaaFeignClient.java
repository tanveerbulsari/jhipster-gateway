package com.jhipster.gateway.web.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jhipster.gateway.dto.UserDto;

@FeignClient(name = "jhipsteruaa")
@RequestMapping("/api")
public interface UaaFeignClient {

	@PostMapping("/register")
	void registerUser(@RequestBody UserDto user);

}
